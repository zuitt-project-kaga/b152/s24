db.users.find(
  {
    $or: [
      { firstname: { $regex: "y", $options: "$i" } },
      { lastname: { $regex: "y", $options: "$i" } },
    ],
  },
  { _id: 0, email: 1, isAdmin: 1 }
);

db.users.find(
  {
    $and: [
      { firstname: { $regex: "e", $options: "$i" } },
      { lastname: { $regex: "e", $options: "$i" } },
    ],
  },
  { _id: 0, email: 1, isAdmin: 1 }
);

db.products.find({$and:[
    {name:{$regex:'x',$options:'$i'}},
    {price:{$gte:50000}}
]});

db.products.update({price:{$lt:2000}},{$set:{isActive:false}});

db.products.deleteMany({price:{$gt:20000}});