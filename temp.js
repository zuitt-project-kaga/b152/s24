db.products.insertMany([
  {
    name: "IphoneX",
    price: 30000,
    isActive: true,
  },
  {
    name: "Samsung Galaxy S21",
    price: 51000,
    isActive: true,
  },
  {
    name: "Razer Blackshark V2X",
    price: 2800,
    isActive: false,
  },
  {
    name: "RAKK Gaming Mouse",
    price: 1800,
    isActive: true,
  },
  {
    name: "Razer Mechanical Keyboard",
    price: 4000,
    isActive: true,
  },
]);

db.users.insertMany([
  {
    firstName: "Mary Jane",
    lastName: "Watson",
    email: "mjtiger@gmail.com",
    password: "tigerjackpot15",
    isAdmin: false,
  },
  {
    firstName: "Gwen",
    lastName: "Stacy",
    email: "stacyTech@gmail.com",
    password: "stacyTech1991",
    isAdmin: true,
  },
  {
    firstName: "Peter",
    lastName: "Parker",
    email: "peterWebDev@gmail.com",
    password: "webdeveloperPeter",
    isAdmin: true,
  },
  {
    firstName: "Jonah",
    lastName: "Jameson",
    email: "jjjameson@gmail.com",
    password: "spideyisamenace",
    isAdmin: false,
  },
  {
    firstName: "Otto",
    lastName: "Octavius",
    email: "ottoOctopi@gmail.com",
    password: "docOck15",
    isAdmin: true,
  },
]);

db.users.find({ email: { $regex: "web", $options: "$i" } });

db.users.find({ name: { $regex: "razer" } });
db.users.find({ name: { $regex: "rakk" } });

db.products.find({
  $or: [{ name: { $regex: "x", $options: '$i' } }, { price: { $lte: 10000 } }],
});

db.products.find({
  $and:[
    {name:
      {
        $regex:'razer',$options:'$i'
      }
    },
    {price:
      {$gte:3000}
    }
  ]
})

db.products.find({$and:[{name:{$regex:'razer',$options:'$i'}},{price:{$gte:3000}}]})
db.products.find({$or:[{name:{$regex:'razer',$options:'$i'}},{price:{$gte:3000}}]})
db.users.find({$and:[
  {lastName:{$regex:'w',$options:'$i'}},
  {isAdmin:false}
]})

db.users.find({$and:[
  {
    lastName:{$regex:'a',$options:'$i'}
  },
  {
    firstName:{$regex:'e',$options:'$i'}
  }
]})

db.users.find({},{_id:0,password:0});
db.users.find({isAdmin:false},{_id:0,email:1})
db.products.find({price:{$gte:10000}},{_id:0,name:1,price:1});

